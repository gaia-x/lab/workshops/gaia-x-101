# Gaia-X 101 workshop

Requires python3

## install dependencies

```shell
pip install -r requirements.txt
```

## prepare a .env file

Copy the .env.example file as .env and edit it to put your domain name, cert & private key

```shell
cp .env.example .env
```

Run the notebook cell 1 by 1, don't forget to edit templates to put your infos, especially `templates/participant.j2`


## Contributions

To contribute to this project, please add a pre-commit hook that removes outputs from the Notebook before pushing to Git

```shell
touch .git/hooks/pre-commit
echo -e '#!/bin/sh\njupyter nbconvert --clear-output --inplace gaia-x-101.ipynb' > .git/hooks/pre-commit
```